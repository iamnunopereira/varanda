module.exports = {
  locales: ['br', 'en'],
  defaultLocale: 'br',
  pages: {
    '*': ['common'],
  },
}
