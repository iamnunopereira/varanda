/** @type {import('next').NextConfig} */
const nextTranslate = require('next-translate-plugin')

nextConfig = {
  reactStrictMode: true,
  eslint: {
    dirs: [
      'src/components',
      'src/constants',
      'src/context',
      'src/hooks',
      'src/layouts',
      'src/libs',
      'src/features',
      'src/pages',
      'src/styles',
      'src/utils',
    ],
  },
  productionBrowserSourceMaps: true,
  ...nextTranslate(),
}

module.exports = nextConfig
