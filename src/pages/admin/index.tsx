import { Login, ProjectAdmin } from '@/components'

export default function Admin() {
  const admin = false

  if (admin) return <ProjectAdmin />

  return <Login />
}
