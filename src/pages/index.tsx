import Head from 'next/head'
import { Projects } from '@/types'
import { projects } from '@/__mocks__'
import type { GetServerSideProps } from 'next'
import { Footer, Header, ProjectList } from '@/components'

type HomeTypes = {
  projects: Projects
}

export default function Home({ projects }: HomeTypes) {
  return (
    <>
      <Head>
        <title>Estudio Varanda - Design | Arquitetura | Moda </title>
        <meta name="description" content="Varanda Estudio" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Header />
      <main>
        <ProjectList projects={projects} />
      </main>
      <Footer />
    </>
  )
}

export const getServerSideProps: GetServerSideProps = async () => {
  return {
    props: {
      projects,
    },
  }
}
