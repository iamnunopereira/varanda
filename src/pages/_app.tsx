import '@csstools/normalize.css'
import '@/styles/variables.css'

import Fallback from './fallback'
import { GlobalStyles } from '@/styles/GlobalStyles'
import { ErrorBoundary } from '@/components'
import type { AppProps } from 'next/app'

const FallbackPage = () => <Fallback />

export default function App({ Component, pageProps }: AppProps) {
  return (
    <ErrorBoundary fallback={FallbackPage}>
      <Component {...pageProps} />
      <GlobalStyles />
    </ErrorBoundary>
  )
}
