import Head from 'next/head'
import { Project } from '@/types'
import { projects } from '@/__mocks__'
import type { GetServerSideProps } from 'next'
import { SectionTitle, ProjectCarousel, ProjectCard } from '@/components'

type ProjectTypes = {
  project: Project
}

export default function Project({ project }: ProjectTypes) {
  return (
    <>
      <Head>
        <title>Varanda Estudio</title>
        <meta name="description" content="Varanda Estudio" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <header>
        <SectionTitle as="h1" title={project?.name} />
      </header>
      <main>
        <ProjectCarousel show={4}>
          <ProjectCard img="https://via.placeholder.com/300x400" />
          <ProjectCard img="https://via.placeholder.com/300x400" />
          <ProjectCard img="https://via.placeholder.com/300x400" />
          <ProjectCard img="https://via.placeholder.com/300x400" />
          <ProjectCard img="https://via.placeholder.com/300x400" />
          <ProjectCard img="https://via.placeholder.com/300x400" />
        </ProjectCarousel>
      </main>
      <footer></footer>
    </>
  )
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  const project = projects.find((project) => project.url === context.params?.project) || undefined

  if (!project) {
    return {
      redirect: {
        destination: '/',
        permanent: false,
      },
    }
  }

  return {
    props: {
      project,
    },
  }
}
