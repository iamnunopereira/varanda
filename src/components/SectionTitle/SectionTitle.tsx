import { Container } from './SectionTitle.styles'

type SectionTitleTypes = {
  title: string
  as?: 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6'
}

export const SectionTitle = ({ title, as }: SectionTitleTypes) => {
  const Component = as ? as : 'h2'

  return (
    <Container>
      <Component>{title}</Component>
    </Container>
  )
}
