import styled from '@emotion/styled'

export const Container = styled.div`
  display: flex;
  justify-content: center;
  text-transform: uppercase;

  padding: 6rem 0 3rem 0;

  h1,
  h2,
  h3,
  h4,
  h5,
  h6 {
    font-size: 2.8rem;
    font-weight: 300;
  }
`
