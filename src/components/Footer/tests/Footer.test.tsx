import { Footer } from '../Footer'
import { render } from '@testing-library/react'

describe('<Sample /> spec', () => {
  it('Should match snapshot', () => {
    const { container } = render(<Footer />)

    expect(container).toMatchSnapshot()
  })
})
