import Image from 'next/image'
import { useState } from 'react'
import { AnimatePresence } from 'framer-motion'
import { labels, contactos } from '@/__mocks__'
import { GoChevronUp, GoChevronDown } from 'react-icons/go'
import { AiOutlineInstagram, AiOutlineMail, AiOutlineGlobal, AiOutlinePushpin } from 'react-icons/ai'
import { Contacts, Container, Item, List, ButtonContainer, InfoWrapper } from './Footer.styles'

const contactsAnimation = {
  show: { height: '10rem', transition: { duration: 0.4, ease: 'easeOut' } },
  hide: { height: 0, transition: { duration: 0.1 } },
}

export const Footer = () => {
  const [toggle, setToggle] = useState(false)

  return (
    <Container>
      <List>
        <Item>
          <a onClick={() => setToggle((toggle) => !toggle)}>
            <ButtonContainer>
              {labels.contactos} <span>{toggle ? <GoChevronDown /> : <GoChevronUp />}</span>
            </ButtonContainer>
          </a>
        </Item>
      </List>
      <AnimatePresence>
        {toggle && (
          <Contacts variants={contactsAnimation} animate="show" exit="hide">
            <Image src="/logo_footer.webp" width="150" height="51" alt="" priority />
            <div>
              <div>
                <InfoWrapper>
                  <AiOutlineMail />
                  <div>{contactos.email}</div>
                </InfoWrapper>
                <InfoWrapper>
                  <AiOutlineInstagram />
                  <div>{contactos.instagram}</div>
                </InfoWrapper>
              </div>
              <div>
                <InfoWrapper>
                  <AiOutlinePushpin />
                  <div>{contactos.morada}</div>
                </InfoWrapper>
                <InfoWrapper>
                  <AiOutlineGlobal />
                  <div>{contactos.localidade}</div>
                </InfoWrapper>
              </div>
            </div>
          </Contacts>
        )}
      </AnimatePresence>
    </Container>
  )
}
