import styled from '@emotion/styled'
import { motion } from 'framer-motion'
import { mq } from '@/styles/mq'

export const Container = styled.footer`
  position: fixed;
  bottom: 0;
  right: 0;
  left: 0;
  background-color: var(--color-brand-ui01);
  padding: 0 1rem 0 1rem;
`

export const List = styled.ul`
  display: flex;
  justify-content: center;
  padding: 1.5rem;
`

export const Item = styled.li`
  text-transform: uppercase;
  font-weight: 600;
  cursor: pointer;
`

export const ButtonContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  span {
    padding-left: 0.5rem;
  }

  svg {
    stroke-width: 1;
  }
`

export const Contacts = styled(motion.div)`
  display: grid;
  grid-template-columns: 17rem auto;
  align-items: center;
  justify-content: center;
  height: 0;
  margin-bottom: 3rem;

  ${mq('s')} {
    grid-template-columns: auto;

    img {
      display: none;
    }
  }
`

export const InfoWrapper = styled.div`
  display: grid;
  grid-template-columns: 2rem auto;
  align-items: center;
  padding-bottom: 0.4rem;
`
