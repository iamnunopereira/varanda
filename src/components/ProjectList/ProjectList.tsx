import Link from 'next/link'
import { Projects } from '@/types'
import { motion } from 'framer-motion'

import { Container, List, Project } from './ProjectList.styles'

export const listAnimation = {
  hidden: { opacity: 1, scale: 0 },
  visible: {
    opacity: 1,
    scale: 1,
    transition: {
      staggerChildren: 0.1,
      delayChildren: 0.2,
    },
  },
}

export const itemAnimation = {
  hidden: { y: 20, opacity: 0 },
  visible: {
    y: 0,
    opacity: 1,
  },
}

type ProjectListTypes = {
  projects: Projects
}

export const ProjectList = ({ projects }: ProjectListTypes) => {
  return (
    <Container>
      <List variants={listAnimation} initial="hidden" animate="visible">
        {projects.map((project) => (
          <motion.li key={project.id} variants={itemAnimation}>
            <Project>
              <Link href={project.url}>{project.name}</Link>
            </Project>
          </motion.li>
        ))}
      </List>
    </Container>
  )
}
