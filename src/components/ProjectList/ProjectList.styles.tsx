import styled from '@emotion/styled'
import { mq } from '@/styles/mq'
import { motion } from 'framer-motion'

export const Container = styled.section`
  margin: 0 6rem 0 6rem;
  padding-bottom: 6rem;

  ${mq('m')} {
    margin: 0 1rem 0 1rem;
  }
`

export const List = styled(motion.ul)`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  gap: 2.4rem 20rem;

  ${mq('xxl')} {
    gap: 2.4rem 12rem;
  }
`

export const Project = styled.h2`
  text-transform: uppercase;
  font-size: 7rem;
  color: var(--color-base-black);

  :hover {
    color: var(--color-brand-ui01);
    cursor: pointer;
  }

  ${mq('m')} {
    font-size: 5rem;
  }
`
