import { Component, isValidElement } from 'react'
import { ElementType, HTMLAttributes, ReactNode } from 'react'

import { Debugger } from '../Debugger'

type ErrorBoundaryProps = {
  children: ReactNode
  debug?: boolean
  fallback?: ElementType
  onError?: (_prop: string, _prop1: string) => void
} & HTMLAttributes<HTMLElement>

type ErrorBoundaryState = {
  error?: string | null
  errorInfo?: {
    componentStack?: string
  } | null
}

export class ErrorBoundary extends Component<ErrorBoundaryProps, ErrorBoundaryState> {
  public static defaultProps = {
    debug: false,
  }

  state: ErrorBoundaryState = { error: null, errorInfo: null }

  componentDidCatch(error: any, errorInfo: any) {
    const { onError } = this.props

    onError?.(error, errorInfo?.componentStack)

    this.setState({ error, errorInfo })
  }

  render() {
    const { error, errorInfo } = this.state
    if (error !== null) {
      const props = {
        error,
        errorStack: errorInfo?.componentStack,
      }

      return [this.props.debug && <Debugger {...props} />, this.getFallbackComponent(props)]
    }

    return this.props.children
  }

  getFallbackComponent = (props: any) => {
    const { fallback: Fallback } = this.props

    if (Fallback && isValidElement(<Fallback />)) {
      return <Fallback />
    } else {
      if (process?.env?.NODE_ENV === 'development') {
        console.log(
          `%c@varanda/error-boundary: `,
          `color:#FDCB82; font-weight:bold;`,
          'Consider to provide a "Fallback" prop',
        )
      }

      return null
    }
  }
}
