import Link from 'next/link'
import Image from 'next/image'
import useTranslation from 'next-translate/useTranslation'
import { useRouter } from 'next/router'
import { Container } from './LanguageSelector.styles'

export const LanguageSelector = () => {
  const { t, lang } = useTranslation('common')
  const router = useRouter()

  return (
    <Container>
      {router?.locales?.map((locale) => {
        if (locale === lang) return null

        const alt = t('language_switch')

        return (
          <Link key={locale} href={router.asPath} locale={locale}>
            <Image src={`/${locale}.png`} alt={alt} width={30} height={30} />
          </Link>
        )
      })}
    </Container>
  )
}
