import Image from 'next/image'

import { Container, Spacer, ImageContainer, ButtonsContainer, Button } from './Login.styles'

const spacerAnimation = {
  show: { height: '0px', transition: { duration: 1, ease: 'easeInOut', delay: 1.8 } },
}

const logoAnimation = {
  show: { opacity: 1, transition: { duration: 2, ease: 'easeInOut' } },
}

const loginAnimation = {
  hidden: { opacity: 0 },
  show: { opacity: 1, transition: { duration: 1, ease: 'easeInOut', delay: 3 } },
}

export const Login = () => {
  return (
    <Container>
      <Spacer variants={spacerAnimation} animate="show"></Spacer>
      <ImageContainer variants={logoAnimation} animate="show">
        <Image src="/LOGO_PRETA.png" width="300" height="102" alt="" priority />
      </ImageContainer>
      <ButtonsContainer>
        <Button variants={loginAnimation} initial="hidden" animate="show">
          Login with Google
        </Button>
      </ButtonsContainer>
    </Container>
  )
}
