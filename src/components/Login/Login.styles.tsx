import styled from '@emotion/styled'
import { motion } from 'framer-motion'

export const Container = styled.section`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 5rem;
`

export const Spacer = styled(motion.div)`
  height: 12rem;
`

export const ImageContainer = styled(motion.div)`
  opacity: 0;
`

export const ButtonsContainer = styled.div`
  margin-top: 8rem;
  display: flex;
  flex-direction: column;
  gap: 1.5rem;
`

export const Button = styled(motion.button)`
  border-width: 0;
  font-family: inherit;
  font-size: 1.4rem;
  font-style: inherit;
  font-weight: inherit;
  font-weight: 600;
  cursor: pointer;
  padding: 1.2rem 0 1.2rem 0;
  width: 28rem;
  text-transform: uppercase;
  border-radius: 1rem;
  color: var(--color-base-black);
  background-color: var(--color-brand-ui01);
  box-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px;
  transition: 0.3s;

  :hover {
    background-color: var(--color-base-black);
    color: var(--color-base-white);
  }
`
