import { useEffect, useState, ReactNode } from 'react'
import { AiFillCaretRight, AiFillCaretLeft } from 'react-icons/ai'

import { Container, ContentWrapper, Wrapper, ButtonLeft, ButtonRight, Content } from './ProjectCarousel.styles'

type ProjectCarouselTypes = {
  show: number
  children: ReactNode[]
}

export const ProjectCarousel = ({ children, show }: ProjectCarouselTypes) => {
  const [length, setLength] = useState(children.length)
  const [touchPosition, setTouchPosition] = useState(null)
  const [currentIndex, setCurrentIndex] = useState(0)

  useEffect(() => {
    setLength(children.length)
  }, [children])

  const next = () => {
    if (currentIndex < length - show) {
      setCurrentIndex((prevState) => prevState + 1)
    }
  }

  const prev = () => {
    if (currentIndex > 0) {
      setCurrentIndex((prevState) => prevState - 1)
    }
  }

  const handleTouchStart = (e: any) => {
    const touchDown = e.touches[0].clientX
    setTouchPosition(touchDown)
  }

  const handleTouchMove = (e: any) => {
    const touchDown = touchPosition

    if (touchDown === null) {
      return
    }

    const currentTouch = e.touches[0].clientX
    const diff = touchDown - currentTouch

    if (diff > 5) {
      next()
    }

    if (diff < -5) {
      prev()
    }

    setTouchPosition(null)
  }

  return (
    <div style={{ padding: '4rem' }}>
      <Container>
        <Wrapper>
          {currentIndex > 0 && (
            <ButtonLeft onClick={prev}>
              <AiFillCaretLeft />
            </ButtonLeft>
          )}
          <ContentWrapper onTouchStart={handleTouchStart} onTouchMove={handleTouchMove}>
            <Content show={show} style={{ transform: `translateX(-${currentIndex * (100 / show)}%)` }}>
              {children}
            </Content>
          </ContentWrapper>
          {currentIndex + (show - 1) < length - 1 && (
            <ButtonRight onClick={next}>
              <AiFillCaretRight />
            </ButtonRight>
          )}
        </Wrapper>
      </Container>
    </div>
  )
}
