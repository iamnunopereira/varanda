import styled from '@emotion/styled'
import { css } from '@emotion/react'

export const Container = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
`

export const Wrapper = styled.div`
  display: flex;
  width: 100%;
  position: relative;
`

export const ContentWrapper = styled.div`
  overflow: hidden;
  width: 100%;
  height: 100%;
`

const Button = css`
  position: absolute;
  z-index: 1;
  top: 50%;
  transform: translateY(-50%);
  width: 48px;
  height: 48px;
  border-radius: 24px;
  background-color: var(--color-brand-ui01);
  border: 1px solid #ddd;

  svg {
    width: 2rem;
    height: 2rem;
  }

  @media (hover: none) and (pointer: coarse) {
    display: none;
  }
`

export const ButtonLeft = styled.button`
  ${Button}
  left: 24px;
`

export const ButtonRight = styled.button`
  ${Button}
  right: 24px;
`

type ContentProps = {
  show: number
}

export const Content = styled.div<ContentProps>`
  display: flex;
  transition: all 250ms linear;
  -ms-overflow-style: none;
  scrollbar-width: none;

  &::-webkit-scrollbar,
  &::-webkit-scrollbar {
    display: none;
  }

  > * {
    flex-shrink: 0;
    flex-grow: 1;

    width: ${({ show }) => `calc(100% / ${show})`};
  }
`
