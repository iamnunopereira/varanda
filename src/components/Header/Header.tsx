import Image from 'next/image'

import { Container } from './Header.styles'

export const Header = () => (
  <Container>
    <div>
      <Image src="/logo_header.webp" width="300" height="102" alt="Estudio Varanda Logo" priority />
    </div>
  </Container>
)
