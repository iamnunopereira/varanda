import styled from '@emotion/styled'

export const Container = styled.header`
  margin: 2rem 0 4rem 0;
  display: flex;
  justify-content: center;
`
