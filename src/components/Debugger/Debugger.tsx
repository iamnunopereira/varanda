import { Container, Details, Message, Summary, Title } from './Debugger.styles'

export type DebuggerPropTypes = {
  error?: string
  errorStack?: string
}

export const Debugger = ({ error, errorStack }: DebuggerPropTypes) => {
  if (process?.env?.NODE_ENV !== 'development') {
    return null
  }

  return (
    <Container>
      <Title>Oops... algo de errado não está certo !</Title>
      <Message>{error?.toString?.()}</Message>
      {errorStack && (
        <Details>
          <Summary>View details</Summary>
          {errorStack}
        </Details>
      )}
    </Container>
  )
}
