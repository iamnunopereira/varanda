import { Container, Image, Overlay, Wrapper } from './ProjectCard.styles'

type ProjectCardTypes = {
  img: string
}

export const ProjectCard = ({ img }: ProjectCardTypes) => {
  return (
    <Wrapper>
      <Container>
        <Image src={img} alt="placeholder" />
        <Overlay />
      </Container>
    </Wrapper>
  )
}
