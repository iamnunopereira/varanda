import styled from '@emotion/styled'

export const Wrapper = styled.div`
  margin: 0 2rem 0 2rem;
`

export const Container = styled.div`
  position: relative;
`

export const Image = styled.img`
  width: 100%;
  border-radius: 2rem;
`

export const Overlay = styled.div`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  height: 100%;
  width: 100%;
  opacity: 0;
  transition: 0.5s ease;
  background-color: var(--color-brand-ui01);
  border-radius: 2rem;
  cursor: pointer;

  :hover {
    opacity: 0.5;
  }
`
