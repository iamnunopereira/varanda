import { Global, css } from '@emotion/react'
import { Inter } from 'next/font/google'

const inter = Inter({ subsets: ['latin'] })

export const GlobalStyles = () => (
  <Global
    styles={css`
      *,
      *::after,
      *::before {
        margin: 0px;
        padding: 0px;
        box-sizing: inherit;
      }

      html {
        line-height: 1.15;
        text-size-adjust: 100%;
        font-size: 62.5%;
        scroll-behavior: smooth;
        font-family: ${inter.style.fontFamily};
      }

      body {
        margin: 0;
        font-size: var(--font-size-base);
        color: var(--color-base-black);
        background-color: var(--color-base-white);
        box-sizing: border-box;
      }

      ul {
        list-style: none;
        padding: 0;
        margin: 0;
      }

      a {
        color: inherit;
        text-decoration: inherit;
      }

      h1 {
        font-size: var(--font-size-h1);
      }

      h2 {
        font-size: var(--font-size-h2);
      }

      h3 {
        font-size: var(--font-size-h3);
      }

      h4 {
        font-size: var(--font-size-h4);
      }

      h5 {
        font-size: var(--font-size-h5);
      }

      h6 {
        font-size: var(--font-size-h6);
      }
    `}
  />
)
