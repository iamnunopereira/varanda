import { Projects } from '@/types'

export const projects: Projects = [
  {
    id: '0001',
    name: 'sakapraia.',
    url: 'sakapraia',
    labels: ['moda', 'direção de arte campanhas', 'packaging', 'rebranding'], // direção de arte campanhas
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
    layout: 3,
    focusedLayout: 1,
    projects: [
      {
        id: '',
        name: '',
        highlightImage: '',
        images: [],
      },
    ],
  },
  {
    id: '0002',
    name: 'scaven.',
    url: 'scaven',
    labels: ['moda', 'direção campanhas', 'packaging'], // direção campanhas
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
    layout: 3,
    focusedLayout: 1,
    projects: [
      {
        id: '',
        name: '',
        highlightImage: '',
        images: [],
      },
    ],
  },
  {
    id: '0003',
    name: 'neuronha.',
    url: 'neuronha',
    labels: ['moda', 'packaging'],
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
    layout: 3,
    focusedLayout: 1,
    projects: [
      {
        id: '',
        name: '',
        highlightImage: '',
        images: [],
      },
    ],
  },
  {
    id: '0004',
    name: 'red life.',
    url: 'red-life',
    labels: ['moda', 'packaging'],
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
    layout: 3,
    focusedLayout: 1,
    projects: [
      {
        id: '',
        name: '',
        highlightImage: '',
        images: [],
      },
    ],
  },
  {
    id: '0005',
    name: 'freaker.',
    url: 'freaker',
    labels: ['moda'],
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
    layout: 3,
    focusedLayout: 1,
    projects: [
      {
        id: '',
        name: '',
        highlightImage: '',
        images: [],
      },
    ],
  },
  {
    id: '0006',
    name: 'movimento.',
    url: 'movimento',
    labels: ['moda', 'rebranding'],
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
    layout: 3,
    focusedLayout: 1,
    projects: [
      {
        id: '',
        name: '',
        highlightImage: '',
        images: [],
      },
    ],
  },
  {
    id: '0007',
    name: 'bali.',
    url: 'bali',
    labels: ['moda', 'rebranding', 'package', 'arquitetura'],
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
    layout: 3,
    focusedLayout: 1,
    projects: [
      {
        id: '001',
        name: 'Moda',
        highlightImage: '/mocks/layout.jpg',
        images: [''],
      },
      {
        id: '002',
        name: 'Rebranding',
        highlightImage: '/mocks/layout.jpg',
        images: [''],
      },
      {
        id: '003',
        name: 'Arquitetura',
        highlightImage: '/mocks/layout.jpg',
        images: [''],
      },
    ],
  },
  {
    id: '0008',
    name: 'Nutrihouse.',
    url: 'nutrihouse',
    labels: ['arquitetura', 'moda'],
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
    layout: 3,
    focusedLayout: 1,
    projects: [
      {
        id: '',
        name: '',
        highlightImage: '',
        images: [],
      },
    ],
  },
  {
    id: '0009',
    name: 'planet pet.',
    url: 'planet-pet',
    labels: ['arquitetura', 'design'],
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
    layout: 3,
    focusedLayout: 1,
    projects: [
      {
        id: '',
        name: '',
        highlightImage: '',
        images: [],
      },
    ],
  },
  {
    id: '0010',
    name: 'apt paiva.',
    url: 'apt-paiva',
    labels: ['arquitetura'],
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
    layout: 3,
    focusedLayout: 1,
    projects: [
      {
        id: '',
        name: '',
        highlightImage: '',
        images: [],
      },
    ],
  },
  {
    id: '0011',
    name: 'apt boa viagem.',
    url: 'apt-boa-viagem',
    labels: ['arquitetura'],
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
    layout: 3,
    focusedLayout: 1,
    projects: [
      {
        id: '',
        name: '',
        highlightImage: '',
        images: [],
      },
    ],
  },
  {
    id: '0012',
    name: 'varanda.',
    url: 'varanda',
    labels: ['arquitetura', 'design'],
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
    layout: 3,
    focusedLayout: 1,
    projects: [
      {
        id: '',
        name: '',
        highlightImage: '',
        images: [],
      },
    ],
  },
  {
    id: '0013',
    name: 'coreto.',
    url: 'coreto',
    labels: ['design'],
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
    layout: 3,
    focusedLayout: 1,
    projects: [
      {
        id: '',
        name: '',
        highlightImage: '',
        images: [],
      },
    ],
  },
  {
    id: '0014',
    name: 'art pe.',
    url: 'art-pe',
    labels: ['branding', 'redes sociais'],
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
    layout: 3,
    focusedLayout: 1,
    projects: [
      {
        id: '',
        name: '',
        highlightImage: '',
        images: [],
      },
    ],
  },
  {
    id: '0015',
    name: 'caveman.',
    url: 'caveman',
    labels: ['branding'],
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
    layout: 3,
    focusedLayout: 1,
    projects: [
      {
        id: '',
        name: '',
        highlightImage: '',
        images: [],
      },
    ],
  },
  {
    id: '0016',
    name: 'jamboo.',
    url: 'jamboo',
    labels: ['arquitetura', 'branding', 'redes sociais'],
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
    layout: 3,
    focusedLayout: 1,
    projects: [
      {
        id: '',
        name: '',
        highlightImage: '',
        images: [],
      },
    ],
  },
  {
    id: '0017',
    name: 'stepple ceramics.',
    url: 'stepple-ceramics',
    labels: ['branding'],
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
    layout: 3,
    focusedLayout: 1,
    projects: [
      {
        id: '',
        name: '',
        highlightImage: '',
        images: [],
      },
    ],
  },
  {
    id: '0018',
    name: 'casa da arvore.',
    url: 'casa-da-arvore',
    labels: ['branding'],
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
    layout: 3,
    focusedLayout: 1,
    projects: [
      {
        id: '',
        name: '',
        highlightImage: '',
        images: [],
      },
    ],
  },
]
