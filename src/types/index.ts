import type { ReactNode } from 'react'

export type PropsWithChildren = {
  children: ReactNode
}

type BrandProjects = {
  id: string
  name: string
  highlightImage: string
  images: string[]
}

export type Project = {
  id: string
  name: string
  url: string
  labels: string[]
  description: string
  layout: number
  focusedLayout: number
  projects: BrandProjects[]
}

export type Projects = Project[]
